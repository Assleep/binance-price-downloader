package svdev.rates.binance.service

import svdev.rates.binance.data.HistoricalKlinesRequest
import svdev.rates.binance.data.KlineData
import java.io.File
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

class SaveDataService {
    fun saveKlinesDataToCSVFile(request: HistoricalKlinesRequest, klinesData: List<KlineData>): File{
        val file = createCsvFile(request)
        file.appendText(klinesData.map { it.getBeautifyKlineData() }.joinToString(separator = ",\n"))
        println("Klines data stored in ${file.name}file")
        return file
    }
    fun createCsvFile(request: HistoricalKlinesRequest): File{
        val fileName = "${request.startTime.atZone(ZoneOffset.UTC).toLocalDate()}-${request.interval.timeInterval}.csv"
        val file = File(fileName)
        if(file.exists()) file.writeText("")
        return file
    }
}