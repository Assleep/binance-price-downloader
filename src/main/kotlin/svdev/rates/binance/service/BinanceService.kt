package svdev.rates.binance.service

import svdev.rates.binance.RetrofitBuilder
import svdev.rates.binance.data.HistoricalKlinesRequest
import svdev.rates.binance.data.KlineData

class BinanceService{
    fun getKlinesData(request: HistoricalKlinesRequest): List<KlineData>{
        Thread.sleep(5000) // To avoid IP ban
        val response = RetrofitBuilder.binanceApiImpl.getKlines(request.getMapOfQueryParameters()).execute()
        if(response.isSuccessful.not()) throw RuntimeException(response.errorBody()?.string())
        return response.body()!!
    }
}