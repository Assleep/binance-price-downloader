package svdev.rates.binance

import svdev.rates.binance.data.HistoricalKlinesRequest
import svdev.rates.binance.data.TimeInterval
import svdev.rates.binance.service.BinanceService
import svdev.rates.binance.service.SaveDataService
import java.time.Instant

fun main(args: Array<String>) {
     val request = HistoricalKlinesRequest(interval = TimeInterval.T1m, startTime = Instant.parse("2021-08-10T10:00:00.904776400Z"))
     val klinesData = BinanceService().getKlinesData(request)
     SaveDataService().saveKlinesDataToCSVFile(request, klinesData)
}