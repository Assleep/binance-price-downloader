package svdev.rates.binance

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap
import svdev.rates.binance.data.KlineData

interface BinanceApiInterface{
   @GET("klines?")
   fun getKlines(@QueryMap parameters: Map<String, String>): Call<List<KlineData>>
}