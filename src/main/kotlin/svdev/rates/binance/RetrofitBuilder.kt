package svdev.rates.binance

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import okhttp3.HttpUrl
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

object RetrofitBuilder {
    private const val BASE_URL = "https://api.binance.com/api/v3/"
    private val mapper = ObjectMapper().registerKotlinModule()

    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create(mapper))
            .build()
    }
    var binanceApiImpl: BinanceApiInterface = getRetrofit().create(BinanceApiInterface::class.java)
}