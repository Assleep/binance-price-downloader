package svdev.rates.binance.data

import java.time.Instant

enum class TimeInterval(val timeInterval: String){
    T1m("1m"), T3m("3m"), T5m("5m"), T15m("15m"), T30m("30m"),
    T1h("1h"), T2h("2h"), T4h("4h"), T6h("6h"), T8h("8h"), T12h("12h"),
    T1d("1d"), T3d("3d"),
    T1M("1M");
}

data class HistoricalKlinesRequest(var interval: TimeInterval,
                                   var base: String = "BTC",
                                   var counter: String = "USDT",
                                   var limit: Int = 1000,
                                   var startTime: Instant,
                                   val endTime: Instant? = null){

   fun getMapOfQueryParameters(): Map<String, String> {
     val mapOfQueryParameters = mutableMapOf(("symbol" to "$base$counter"),
                                        ("limit" to limit.toString()),
                                        ("interval" to interval.timeInterval),
                                        ("startTime" to startTime.toEpochMilli().toString()))
     if (endTime != null) mapOfQueryParameters.put("endTime" , endTime.toEpochMilli().toString())
     return mapOfQueryParameters
   }
}
