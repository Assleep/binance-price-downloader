package svdev.rates.binance.data

import com.fasterxml.jackson.annotation.JsonFormat

@JsonFormat(shape = JsonFormat.Shape.ARRAY)
data class KlineData(val openTime: Long,
                     val open: String,
                     val high: String,
                     val low: String,
                     val close: String,
                     val volume: String,
                     val closeTime: Long,
                     val quoteAssetVolume: String,
                     val numberOfTrades: Int,
                     val takerBuyBaseAssetVolume: String,
                     val takerBuyQuoteAssetVolume: String,
                     val ignore: String){
    fun getBeautifyKlineData(): String {
        return  """Open time: $openTime
   | Open: $open
   | High: $high
   | Low: $low
   | Close: $close
   | Volume: $volume
   | Close time: $closeTime
   | Quote asset volume: $quoteAssetVolume
   | Number of trades: $numberOfTrades
   | Taker buy base asset volume: $takerBuyBaseAssetVolume
   | Taker buy quote asset volume: $takerBuyQuoteAssetVolume""".trimMargin()
    }
}

