import io.mockk.every
import io.mockk.mockk
import okhttp3.ResponseBody
import org.junit.jupiter.api.*
import retrofit2.Response
import svdev.rates.binance.RetrofitBuilder
import svdev.rates.binance.data.HistoricalKlinesRequest
import svdev.rates.binance.service.BinanceService

@TestInstance(TestInstance.Lifecycle.PER_CLASS )
class GetKlinesDataWithResponseErrorTest{
    @Test
    fun HTTPResponseCode500(){
        val retrofit = mockk<RetrofitBuilder>(relaxed = true)
        val historicalKlinesRequest = mockk<HistoricalKlinesRequest>(relaxed = true)
        every{retrofit.binanceApiImpl.getKlines(mapOf()).execute()} returns Response.error(500, ResponseBody.create(null, ""))
        assertThrows<RuntimeException> {  BinanceService().getKlinesData(historicalKlinesRequest)}
    }
    @Test
    fun HTTPResponseCode429(){
        val retrofit = mockk<RetrofitBuilder>(relaxed = true)
        val historicalKlinesRequest = mockk<HistoricalKlinesRequest>(relaxed = true)
        every{retrofit.binanceApiImpl.getKlines(historicalKlinesRequest.getMapOfQueryParameters()).execute()} returns Response.error(429, ResponseBody.create(null, ""))
        assertThrows<RuntimeException> {  BinanceService().getKlinesData(historicalKlinesRequest) }
    }
}