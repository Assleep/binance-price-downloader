import io.mockk.MockKException
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import svdev.rates.binance.RetrofitBuilder
import svdev.rates.binance.data.HistoricalKlinesRequest
import svdev.rates.binance.data.TimeInterval
import svdev.rates.binance.service.BinanceService
import java.time.Instant

class GetKlinesDataWithoutResponseTest {
    @Test
    fun noResponse(){
        val retrofit = mockk<RetrofitBuilder>(relaxed = true)
        every { retrofit.binanceApiImpl.getKlines(mapOf()).execute() } returns null
        assertThrows<RuntimeException> { BinanceService().getKlinesData(HistoricalKlinesRequest(startTime = Instant.now(), interval = TimeInterval.T1m)) }
    }
}